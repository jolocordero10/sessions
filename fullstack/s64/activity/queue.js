let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {
    collection.push(element);
    return collection;
}

function dequeue() {
    if (collection.length === 0) return collection;

    collection.shift();
    return collection;
}

function front() {
    if (collection.length === 0) return undefined;

    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};