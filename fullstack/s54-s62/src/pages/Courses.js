// import coursesData from '../data/coursesData.js';
import { useEffect, useState, useContext } from 'react';
// import CourseCard from '../components/CourseCard.js';
import AdminView from '../components/AdminView'
import UserView from '../components/UserView'
import UserContext from '../UserContext';

export default function Courses(){

	const {user} = useContext(UserContext);

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setCourses(data);
		})
	}, []);

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data)
		})
	}

	useEffect(() => {
		fetchData();
	}, []);

	// const courses = coursesData.map(course_item => {
	// 	return (
	// 		<CourseCard key={course_item.id} course={course_item}/>
	// 	)
	// })

	return(
		<>
			<h1>Courses</h1>
			{user.isAdmin? 
				<AdminView coursesData={courses} fetchData={fetchData}/>
				:
				<UserView coursesData={courses}/>
			}
		</>
	)
} 
