import Banner from '../components/Banner.js';
import FeaturedCourses from '../components/FeaturedCourses.js'
import Highlights from '../components/Highlights.js';

export default function Home() {
	return (
		<>
	        <Banner
              title="Earl's Fishing Academy"
              subtitle="Fish for everyone!"
              buttonText="Enroll now!"
              buttonLink="/courses"
            />
            <FeaturedCourses/>
	        <Highlights/>
		</>
	)
}