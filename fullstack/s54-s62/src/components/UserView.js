import { useState, useEffect } from 'react';
import CourseCard from './CourseCard';
import CourseSearch from './CourseSearch';
import SearchByPrice from './SearchByPrice';


export default function UserView({ coursesData }) {

    const [courses, setCourses] = useState("");

   useEffect(() => {
        const activeCourses = coursesData.filter(course => course.isActive);

        setCourses(activeCourses.map(courseItem => (
            <CourseCard key={courseItem._id} course={courseItem} />
        )));
    }, [coursesData]);

    return (
            <div>
                <h1 className="text-center">Courses</h1>
                <CourseSearch />
                <SearchByPrice />
                {courses}
            </div>
        );
}