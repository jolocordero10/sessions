import React, { useState, useEffect } from 'react';

const UpdateProfile = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [message, setMessage] = useState('');
  const [isActive, setIsActive] = useState(false);

  const handleUpdate = async (e) => {
    e.preventDefault();

    try {
      const token = localStorage.getItem('token'); // Replace with your actual JWT token
      const response = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          mobileNo: mobileNo,
        }),
      });

      if (response.ok) {
        setMessage('Profile updated successfully');
        setFirstName('');
        setLastName('');
        setMobileNo('');
      } else {
        const errorData = await response.json();
        setMessage(errorData.message);
      }
    } catch (error) {
      setMessage('An error occurred. Please try again.');
      console.error(error);
    }
  };

useEffect(() => {
    if(firstName !== "" && lastName !== "" && mobileNo !== ""){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [firstName, lastName, mobileNo]);


  return (
    <div className="container">
      <h2 className="pt-5">Update Profile</h2>
      <form onSubmit={handleUpdate}>
        <div className="mb-3">
          <label htmlFor="firstName"className="form-label">
            First Name:
          </label>
            <input
              type="text"
              className="form-control"
              name="firstName"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
        </div>
        <div className="mb-3">
          <label htmlFor="lastName"className="form-label">
            Last Name:
            </label>
            <input
              type="text"
              className="form-control"
              name="lastName"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
        </div>
        <div className="mb-3">
          <label htmlFor="mobileNo"className="form-label">
            Mobile No:
          </label>
            <input
              type="text"
              className="form-control"
              name="mobileNo"
              value={mobileNo}
              onChange={(e) => setMobileNo(e.target.value)}
            />
        </div>
        {message && <div className="alert alert-danger">{message}</div>}
        <button type="submit" className="btn btn-primary" disabled={isActive === false}>
          Update Profile
        </button>
      </form>
    </div>
  );
}

export default UpdateProfile;