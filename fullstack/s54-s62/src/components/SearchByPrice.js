import React, { useState } from 'react';

export default function CourseSearch() {
    const [minPrice, setMinPrice] = useState(0);
    const [maxPrice, setMaxPrice] = useState(0);
    const [searchResults, setSearchResults] = useState([]);

    const handleSearch = () => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/searchByPrice`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({ minPrice: minPrice,maxPrice: maxPrice }),
        })
        .then(res => res.json())
        .then(data => {
            setSearchResults(data.courses);
        });
    };

    console.log(searchResults.courses)

    return (
        <div>
            <h1>Search Courses by Price Range</h1>
            <div className="form-group">
                <label>Min Price:</label>
                <input
                    type="number"
                    className="form-control"
                    value={minPrice}
                    onChange={e => setMinPrice(e.target.value)}
                />
            </div>
            <div className="form-group">
                <label>Max Price:</label>
                <input
                    type="number"
                    className="form-control"
                    value={maxPrice}
                    onChange={e => setMaxPrice(e.target.value)}
                />
            </div>
            <button className="btn btn-primary" onClick={handleSearch}>
            Search
            </button>

            <h2>Search Results:</h2>
            <ul>
                {searchResults.map(course => (
                    <li key={course._id}>{course.name}</li>
                ))}
            </ul>
        </div>
    );
};