// 1.
function getUserInfo(){
	let person = {
		name: "John Doe",
		age: 25,
		address: "123 Street, Quezon City",
		isMarried: false,
		petName: "Danny"
	};

	return person; 
}

console.log(getUserInfo())

// 2.
function getArtistsArray(){
	let artists = ["Syd Hartha", "Dilaw", "The Ridleys", "Because", "Cup Of Joe"];

	return artists;
}

console.log(getArtistsArray());

// 3.
function getSongsArray(){
	let songs = ["Tila Tala", "Uhaw", "Aphrodite", "BMW", "Pasilyo"];

	return songs;
}

console.log(getSongsArray());

// 4.
function getMoviesArray(){
	let movies = ["A Serbian Film", "Human Centipede", "Your Name", "My Neighbor Totoro", "Perfect Blue"];

	return movies;
}

console.log(getMoviesArray());

// 5.
function getPrimeNumbersArray(){
	let primeNumbers = [2, 3, 5, 7, 17];

	return primeNumbers;
}

console.log(getPrimeNumbersArray());

