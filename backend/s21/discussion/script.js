// FUNCTION DECLARATION & INVOCATION
// function printName(){
// 	console.log("My name is Jeff");
// }

// printName();

// // FUNCTION EXPRESSION
// let variable_function = function(){
// 	console.log("Hello from function expression!")
// }

// variable_function();

// SCOPING
let global_variable = "Call me mr. worldwide";

console.log(global_variable);

function showNames(){
	let function_variable = "Joe";
	const function_const = "John";

	console.log(function_variable);
	console.log(function_const);

	// You CAN use global variables inside any function as long as they are declared outside of the function scope.
	console.log(global_variable);
}

// You CANNOT use locally-scoped variables outside the function they are declared in.
// console.log(function_variable);

showNames();


// NESTED FUNCTIONS
function parentFunction(){
	let name = "Jane";

	function childFunction(){
		let nested_name = "John"

		console.log(name);

		// Accessing the 'nested_name' variable within the same function it was declared in, WILL work.
		console.log(nested_name);
	}

	childFunction();

	// Accessing the 'nested_name' variable OUTSIDE the function it was declared in, WILL NOT work.
	// console.log(nested_name);
}

parentFunction();

// Invoking the 'childFunction' outside the function it was declared in will NOT work.
// childFunction();


// BEST PRACTICE FOR FUNCTION NAMING
// -Function names can be as long as you want as long as it is descriptive. Avoid lazy naming conventions that don't describe what the function actually does.
// function printWelcomeMessageForUser(){
// 	let first_name = prompt("Enter your first name: ");
// 	let last_name = prompt("Enter your last name: ");

// 	console.log("Hello, " + first_name + " " + last_name + "!");
// 	console.log("Welcome sa page ko!");
// }

// printWelcomeMessageForUser();


// RETURN STATEMENT 
// -Sets a value for a function which can then be used later on.
function fullName(){
	console.log("Hello Earl!")
}

console.log(fullName());