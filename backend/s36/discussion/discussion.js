// [Section] MongoDB Aggregation
/*
  - Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data
  - Compared to doing CRUD operations on our data from previous sessions, aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisions without having to create a frontend application.
*/

// using the aggregate method
/*
  - The "$" symbol will refer to a field name that is available in the documents that are being aggregated on.
*/
db.fruits.aggregate([
  // $match - used to match or get documents that satisfies the condition
  // $match is similar to find(). You can use query operaators to make your criteria more flexible
  /*
    Syntax
      - { $match: { field: value } }
  */
  /*
    $match -> Apple, Kiwi, Banana
  */
  { $match: { onSale: true }},
  // $group - allows us to group together documents and create an analysis oput of the group elements
  /*
    Syntax
    - { $group: { _id: "value", fieldResult: "valueResult" } }
  */
  // _id: $supplier_id
  /*
    Apple = 1.0
    Kiwi = 1.0
    Banana = 2.0

    _id: 1.0
      Apple, Kiwi

      total: sum of the fruit stock of 1.0
      total: Apple stocks + Kiwi Stocks
      total: 20           + 25
      total: 45

    _id: 2.0
      Banana

      total: sum of the fruit stock of 2.0
      total: Banana stocks
      total: 15
  */
  // $sum - used to add or total the values in a given field
  { $group: { _id: "$supplier_id", total: { $sum: "$stock"}}}
]);

// Field projection with aggregation

db.fruits.aggregate([
  /*
    $match - Apple, Kiwi, Banana
  */
  { $match: { onSale: true }},
  /*
    Apple - 1.0
    Kiwi - 1.0
    Banana - 2.0

    _id: 1.0

      avgStocks: average stocks of fruits in 1.0
      avgStocks: (Apple Stocks + Kiwi Stocks) / 2
      avgStocks: (20           + 25         ) / 2
      avgStocks: 45 / 2
      avgStocks: 22.5

    _id: 2.0

      avgStocks: avergae stocks of fruits in 2.0
      avgStocks: Banana Stocks / 1
      avgStocks: 15 / 1
      avgStocks: 15
  */
  /*
    {
      _id: 1,
      avgStocks: 22.5
    }
    {
      _id: 2,
      avgStocks: 15
    }
  */
  // $avg - gets the average of the values of the given field per group
  { $group: { _id: '$supplier_id', avgStocks: { $avg: '$stock'}}},
  // $project - can be used when "aggregating" data to include/exclude fields from the returned results (Field Projection)
  /*
    Syntax
    - { $project : { field: 1/0 } }
  */
  /*
    {
      avgStocks: 22.5
    }
    {
      avgStocks: 15
    }
  */
  { $project: {_id: 0 }}
]);

// Sorting aggregated results

db.fruits.aggregate([
  /*
    $match - Apple, Kiwi, Banana
  */
  { $match: { onSale: true }},
  /*
    Apple - 1.0
    Kiwi - 1.0
    Banana - 2.0

    _id: 1.0

      maxPrice: finds the highest price of fruit
      maxPrice: Apple Price vs Kiwi Price
      maxPrice: 40          vs 50
      maxPrice: 50

    _id: 2.0

      maxPrice: finds the highest price of fruit
      maxPrice: Banana Price
      maxPrice: 20
      maxPrice: 20
  */
  /*
    {
      _id: 1,
      maxPrice: 50
    }
    {
      _id: 2,
      maxPrice: 20
    }
  */
  { $group: { _id: '$supplier_id', maxPrice: { $max: '$price'}}},
  // $sort - to change the order of the aggregated result
  // Providing a value of -1 will sort the aggregated results in a reverse order
  /*
    Syntax
    - { $sort { field: 1/-1 } }
  */
  { $sort: { maxPrice: -1 }}
]);

// Aggregating results based on array fields
db.fruits.aggregate([
  // $unwind - deconstruct the array field from a collection with an array value to output result for each element
  /*
    Syntax
    - { $unwind: field }
  */
  { $unwind: "$origin"}
]);

// Displays fruit documents by their origin and the kinds of fruits that are supplied
db.fruits.aggregate([
  { $unwind : "$origin" },
  { $group : { _id : "$origin" , kinds : { $sum : 1 } } }
]);