// ARGUMENTS AND PARAMETERS
function printName(name){
	console.log("I'm the real " + name);
}

printName("Slim Shady");

function checkDivisibilityBy2(number){
	let result = number % 2;

	console.log("The remainder of " + number + " is " + result);
}

checkDivisibilityBy2(15);


// MULTIPLE ARGUMENTS AND PARAMETERS
// You can use multiple arguments and parameters as long as they are equal. If the number of arguments don't match the number of parameters, javascript will set the value of that parameter as undefined.
// NOTE: The positioning of each parameter is crucial as to which data it will receive.
function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Juan", "Dela", "Cruz");


// USAGE OF PROMPTS AND ALERTS
// 1. Get data from prompt
let user_name = prompt("Enter your username:");


function displayWelcomeMessageForUser(userName){ //3. The userName parameter gets the data passed as argument

	// 4. The data from the userName parameter is then used in the alert
	alert("Welcome back to Valorant " + userName);
}

// 2. Pass data from user_name variable as argument
displayWelcomeMessageForUser(user_name);